DOCKER_DIR="docker"
CONTAINER_REPO="registry.gitlab.com/pinimo/musicology_thesis_template:2019"
CONTAINER_NAME=$(CONTAINER_REPO) # "musithesis"
DOCKER_USER=thesis
MAIN_FILE="ClassicThesis"
TTY_FLAGS="-it"
ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

compile:
	$(MAKE) run CMD="lualatex --shell-escape $(MAIN_FILE).tex" TTY_FLAGS=""

compile-twice:
	$(MAKE) clean
	$(MAKE) compile
	$(MAKE) bibtex
	$(MAKE) compile

docker-build:
	docker build $(DOCKER_DIR) -t $(CONTAINER_NAME) \
		--build-arg USER=$(DOCKER_USER) \
		-f $(DOCKER_DIR)/Dockerfile

sources:
	([ -d texmf/tex/latex/lyluatexmp ] \
		|| (mkdir -p texmf && mkdir -p texmf/tex && mkdir -p texmf/tex/latex \
			&& cd texmf/tex/latex/ \
			&& git clone https://github.com/pinimo/lyluatexmp.git \
			&& cd lyluatexmp \
			&& git checkout multipage \
			&& cd ../../../..))
	([ -d texmf/tex/latex/lyluatex ] \
		|| (mkdir -p texmf && mkdir -p texmf/tex && mkdir -p texmf/tex/latex \
			&& cd texmf/tex/latex/ \
			&& git clone https://github.com/jperon/lyluatex.git \
			&& cd ../../..))

build:
	$(MAKE) docker-build
	$(MAKE) sources

run:
	docker run $(TTY_FLAGS) -v "$(ROOT_DIR)":"/home/$(DOCKER_USER)/files" \
		-v "$(ROOT_DIR)/texmf":"/home/$(DOCKER_USER)/texmf" \
		--user="$(DOCKER_USER)" \
		$(CONTAINER_NAME) $(CMD)

clean:
	rm -f *.aux *.log *.out *.toc *.tcp *.lol *.lof *.tps *.lot *.run.xml \
		*.loxmp *.blg *.bbl *-blx.bib
	rm -f .lesshst .bash_history .sudo_as_admin_successful
	rm -rf tmp-ly/

bibtex:
	$(MAKE) run CMD="bibtex $(MAIN_FILE)"

push:
	$(MAKE) build CONTAINER_NAME=$(CONTAINER_REPO) \
		&& docker push $(CONTAINER_REPO)

pull:
	docker pull $(CONTAINER_REPO)
