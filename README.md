## What is this

This repository provides a template of musicological thesis, based upon
André Miede's template [classicthesis](https://bitbucket.org/amiede/classicthesis/) with
[LuaLaTeX](http://luatex.org/) and [LilyPond](http://lilypond.org), with
insertion managed through TeX packages
[lyluatex](https://github.com/jperon/lyluatex) and
[lyluatexmp](https://github.com/uliska/lyluatexmp). I am also distributing a
minimal Docker image through the project's
[Gitlab Container Registry](https://gitlab.com/pinimo/musicology_thesis_template/container_registry).

I am most grateful to all authors and contributors of the free software
making this template possible to develop.

## Getting started

- Clone the git repository: `$ git clone https://gitlab.com/pinimo/musicology_thesis_template/`

- Download the Docker image (provided Docker is installed, running and you have already logged in with `$ docker login`): `$ make pull`

- Compile everything (twice + bibtex): `make compile-twice`

## License
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; see the file COPYING.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
